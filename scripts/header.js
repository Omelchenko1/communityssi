var mobileWrapper = document.getElementById('mobile-menu-wrapper'),
    mobileMenu = document.getElementById('mobile-menu');

document.querySelector('.mobile-menu-btn').addEventListener('click', function () {
  mobileWrapper.classList.add('active');
  mobileMenu.classList.add('active');
});

mobileWrapper.addEventListener('click', function (e) {
  if(e.target.matches('#mobile-menu-wrapper')) {
    mobileWrapper.classList.remove('active');
    mobileMenu.classList.remove('active');
  }
});

var tablinks = [].slice.call(document.querySelectorAll('.tabset__item'));

tablinks.forEach(function (item) {
  item.addEventListener('click', function (e) {
    e.preventDefault();

    var id = item.getAttribute('href').slice(1);

    tablinks.forEach(function (item) {
      item.classList.remove('active');
    });

    this.classList.add('active');

    var tabs = [].slice.call(document.querySelectorAll('.tabs-holder .tab'));

    tabs.forEach(function (item) {
      item.classList.remove('tab-active');
    });

    document.getElementById('' + id).classList.add('tab-active');
  });
});
